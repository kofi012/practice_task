# Bash Script, Variables, Arguments and conditions

This will be a small demo/ code along for use to learn how to use variables, arguments and conditions

By the end we'll have a small script that uses these to install nginx on an ubuntu machine when given an IP as an argument.

We'll talk about the difference between arguments and variables.

We will also look into using conditions to help us run our script - using control flow and handling errors.

## To cover

- script basics
- running scripts
- escape notations
- arguments
- variables and user input
- conditions

### Script basics

Script is several bash commands written in a file that a bash interpreter can execute.
This is useful to automate tasks and others.

### running script

To run a script, it must first be executable. Check its permission using `ll` or `ls` `-l`. it should have `x` to be executed.

Add the permission "execute by `chmod +x <file>`

You might need to do this in a remote machine as well when moving/creating new files on the fly.

You can run the file by just pointing to it or calling `bash <file>`

```bash
# just pointing to file
./var_bash.sh

# to run the file remotely call it using bash
bash ./var_bash.sh

# the dot just represents "HERE", you could put the entire 
```
```bash
# Argument
data that a function can take in and use internally
touch <argument>
or
$ mv <argument1> <argument2>

In script if you want to use arguments they are define with $#, where # is a number.
for example

echo $1
echo $1
echo $1

# Interpolation of variable into a string

echo "this is argument 1: $1"
echo "this is argument 1: $2"
echo "this is argument 1: $3"
echo "thos is argument 1: $4" "will you break?"
```
Imagine you want to make a function, or script that takes in any number of arguments and does something?

You can use `$*` to represent all the given arguments in a list.

To explain the above we need loops!

### Loops

A loop is a block of code that runs for a given number of times.
This can be useful if you have repetitive task

```bash
for x in [item item2 item3]
do
echo "Block of code"
echo $x
done
```
In a loop, a program iterates over an iteratable object (usually a list), substituting

### Conditions

### Escape notation

In bash and other languages, you have certain character that behave/signal important things other than the actual symbol. For example `""` are used to outline a string of character.

How do you print out/echo out
ff